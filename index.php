<?php require_once('head.php');?>
  <body>
    <header>
        <img class="logo" src="logo.png" alt="logo CoolBlog" width="200" height="110" />
        <?php include('menu.php');?>
    </header>
    
    <div class="content">
    
        <article id="main">
            <img src="main_article.png" />
            <h1>Jak se staví webová stránka?!</h1>
            <p>Lorem ipsum dolor sit amet consectetuer et neque et tellus condimentum. Volutpat turpis Sed tortor a Aenean ridiculus faucibus pede accumsan pellentesque. Sed vitae lorem libero sed.</p>
            <p>Quisque nascetur sed Nullam at urna Sed purus accumsan commodo est. Porttitor Quisque sed metus Suspendisse vitae volutpat orci mauris orci Proin. Phasellus congue pellentesque ut et.</p>
            <p class="link"><a href="">Přečíst si více</a></p>
         </article>
         
        <div class="row">

            <?php

            $stmt = $conn->prepare("SELECT titulek, obsah, jmeno, prijmeni FROM clanky JOIN autor ON clanky.idautor = autor.idautor ORDER BY idclanky DESC LIMIT 3");
            $stmt->execute();


            while ($row = $stmt->fetch()) {
                echo '<article class="minor">';
                echo '<img src="article.png" />';
                echo '<h2>'.$row['titulek'].'</h2>';
                echo '<p>'.$row['jmeno'].' '.$row['prijmeni'].'</p>';
                echo '<p>'.$row['obsah'].'</p>';
                echo '<p class="link"><a href="">Přečíst si více</a></p>';
                echo '</article>';
            }
            ?>
         </div>
    </div>
    <?php include('footer.php');?>

  </body>
</html>